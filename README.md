# Termite Configuration

Termite configuration (fizzy-compliant)

## Documentation

See [wiki](https://gitlab.com/fizzycfg/official-cfg/configs-termite/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)
